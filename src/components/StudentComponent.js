import React from "react";
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import StudentService from "../services/StudentService";


class StudentComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            students: [],
            form: {
                name: "",
                email: "",
                dob: "",
            },
        }
    }

    componentDidMount() {
        StudentService.getStudents().then((response) => {
            this.setState({ students: response.data })
        });
    }

    mostrarModalInsertar = () => {
        this.setState({
            modalInsertar: true,
        });
    };

    cerrarModalInsertar = () => {
        this.setState({ modalInsertar: false });
    };

    insertar = () => {
        StudentService.registerNewStudents(this.state.form).then((response) => {
            console.log(response.data)
        });
        StudentService.getStudents().then((response) => {
            this.setState({ students: response.data })
        });
        this.setState({ modalInsertar: false });
    }

    eliminar = (id) => {
        var opcion = window.confirm("Estás Seguro que deseas Eliminar el elemento " + id);
        if (opcion == true) {
            StudentService.deleteStudents(id).then((response) => {
                console.log(response.data)

            });
        }
    };

    mostrarModalActualizar = (student) => {
        this.setState({
            form: student,
            modalActualizar: true,
        });
    };

    cerrarModalActualizar = () => {
        this.setState({ modalActualizar: false });
    };

    editar = (dato) => {
        StudentService.updateStudents(dato.id, dato.name, dato.email)
        this.setState({ modalActualizar: false });
      };

    
    handleChange = (e) => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            },
        });
    };

    render() {
        return (
            <div>
                <br />
                <div>
                    <h1 className="text-center"> Lista de Estudiantes</h1>
                </div>
                <br />
                <Container>
                    <br /><br />
                    <Button color="success" onClick={() => this.mostrarModalInsertar()}>Agregar Nuevo Estudiante</Button>
                    <Table>
                        <thead>
                            <tr>
                                <th> Id Estudiante</th>
                                <th> Nombre del Estudiante</th>
                                <th> Email Estudiante</th>
                                <th> Fecha de Nacimiento de Estudiante</th>
                                <th> Edad Estudiante</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.students.map(
                                    student =>
                                        <tr key={student.id}>
                                            <td>{student.id}</td>
                                            <td>{student.name}</td>
                                            <td>{student.email}</td>
                                            <td>{student.dob}</td>
                                            <td>{student.age}</td>
                                            <td><Button color="primary" onClick={() => this.mostrarModalActualizar(student)}>Editar</Button>{"  "}<Button color="danger" onClick={() => this.eliminar(student.id)}>Eliminar</Button></td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </Table>
                </Container>

                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Insertar Estudiante</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Nombre:
                            </label>
                            <input
                                className="form-control"
                                name="name"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Email:
                            </label>
                            <input
                                className="form-control"
                                name="email"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Fecha de Nacimiento:
                            </label>
                            <input
                                className="form-control"
                                name="dob"
                                type="date"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                    </ModalBody>

                    <ModalFooter>
                        <Button
                            color="primary"
                            onClick={() => this.insertar()}
                        >
                            Insertar
                        </Button>
                        <Button
                            className="btn btn-danger"
                            onClick={() => this.cerrarModalInsertar()}
                        >
                            Cancelar
                        </Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modalActualizar}>
                    <ModalHeader>
                    <div><h3>Editar Registro</h3></div>
                    </ModalHeader>

                    <ModalBody>
                        <FormGroup>
                        <label>
                        Id:
                        </label>
                        
                        <input
                            className="form-control"
                            readOnly
                            type="text"
                            value={this.state.form.id}
                        />
                        </FormGroup>
                        
                        <FormGroup>
                        <label>
                            Nombre: 
                        </label>
                        <input
                            className="form-control"
                            name="name"
                            type="text"
                            onChange={this.handleChange}
                            value={this.state.form.name}
                        />
                        </FormGroup>
                        
                        <FormGroup>
                        <label>
                            Email: 
                        </label>
                        <input
                            className="form-control"
                            name="email"
                            type="text"
                            onChange={this.handleChange}
                            value={this.state.form.email}
                        />
                        </FormGroup>                                  
                    </ModalBody>

                    <ModalFooter>
                        <Button
                        color="primary"
                        onClick={() => this.editar(this.state.form)}
                        >
                        Editar
                        </Button>
                        <Button
                        color="danger"
                        onClick={() => this.cerrarModalActualizar()}
                        >
                        Cancelar
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default StudentComponent