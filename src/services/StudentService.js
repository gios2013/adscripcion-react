import axios from "axios";

const STUDENTS_REST_API_URL = 'http://localhost:8080/api/v1/student';

class StudentService {

    getStudents(){
        return axios.get(STUDENTS_REST_API_URL);
    }

    registerNewStudents(student){
        return axios.post(STUDENTS_REST_API_URL, student);
    }

    deleteStudents(studentId){
        console.log(studentId);
        return axios.delete(STUDENTS_REST_API_URL+'/'+ studentId);
    }

    updateStudents(studentId, name, email){
        console.log(studentId, name, email)
        return axios.put(STUDENTS_REST_API_URL+'/'+ studentId+'?name='+name+'&email='+email)
    }
}

export default new StudentService();